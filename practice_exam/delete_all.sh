minikube ssh 'sudo rm  -f /data/*'
minikube ssh 'sudo rm  -f /mnt/data/*'
minikube kubectl -- delete all --all
minikube kubectl -- delete ns ckad-ns1
minikube kubectl -- delete ns ckad-ns2
minikube kubectl -- delete ns ckad-ns3
minikube kubectl -- delete ns ckad-ns4
minikube kubectl -- delete ns ckad-ns5
minikube kubectl -- delete ns ckad-ns6
minikube kubectl -- delete ns ckad-ns7
minikube kubectl -- delete ns ckad-ns8
minikube kubectl -- delete ns ckad-ns9
minikube kubectl -- delete ns ckad-ns10
minikube kubectl -- delete ns ckad-ns11
minikube kubectl -- delete ns  ckad-ex10
minikube kubectl -- delete ns  ckad-ex11
minikube kubectl -- delete ns  ckad-ex12
minikube kubectl -- delete ns  ckad-ns12
minikube kubectl -- delete ns  default
minikube kubectl -- delete ns  ex10
minikube kubectl -- delete ns  ingress-nginx
minikube kubectl -- delete ns  limited
docker image rm nginx-1 nginx-2
